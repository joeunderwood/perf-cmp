WRK := ./.perf/wrk
 

wrk:
	$(WRK) -c 16 -t 16 -d 60s -R 16 -s .perf/post.lua http://localhost:8000/

tank:
	docker run  --cpus="2" --rm -v $$(pwd)/.perf:/var/loadtest --net host -it direvius/yandex-tank
