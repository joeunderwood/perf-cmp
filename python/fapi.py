from typing import Optional

from fastapi import FastAPI
from logic import get_data

app = FastAPI()


@app.post("/")
def read_item(
    country: Optional[str] = None,
    lang: Optional[str] = "en",
    campaigns: Optional[str] = None,
    showcase: Optional[str] = "us",
    platform: Optional[str] = None,
):
    # return get_data(country=country, lang=lang, campaigns=campaigns, showcase=showcase, platform=platform)
    return "ok"
