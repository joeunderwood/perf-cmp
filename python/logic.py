import json
import hashlib


with open("../data.json") as f:
    data = f.read()
    showcase = json.loads(data)


def dict_hash(dictionary) -> str:
    dhash = hashlib.md5()
    encoded = json.dumps(dictionary, sort_keys=True).encode()
    dhash.update(encoded)
    return dhash.hexdigest()


def get_data(country=None, lang="en", campaigns=None, showcase="us", platform=None):
    # showcase = json.loads(data)

    return dict_hash(showcase)
