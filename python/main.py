#!/usr/bin/env python

import argparse
import json
import time
from http.server import HTTPServer, BaseHTTPRequestHandler
from logic import get_data


class Server(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def do_POST(self):
        start = time.time()
        content_length = int(self.headers.get("Content-Length", 0))
        post_data = self.rfile.read(content_length)
        self._set_headers()

        try:
            post_data = json.loads(post_data.decode("utf-8"))
        except ValueError:
            post_data = {}
        self.wfile.write(b"ok")
        return

        data = get_data(**post_data)

        self.wfile.write(data.encode("utf-8"))

        print("t", time.time() - start, data)


def run(server_class=HTTPServer, handler_class=Server, addr="localhost", port=8000):
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)

    print(f"Starting httpd server on {addr}:{port}")
    httpd.serve_forever()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run a simple HTTP server")
    parser.add_argument(
        "-l",
        "--listen",
        default="localhost",
        help="Specify the IP address on which the server listens",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Specify the port on which the server listens",
    )
    args = parser.parse_args()
    run(addr=args.listen, port=args.port)
