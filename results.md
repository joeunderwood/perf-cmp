# Just json post
### Py3.8 simple http
2000rps, resp time 1-8ms
https://overload.yandex.net/423684

### Py3.8 fastapi + 1proc uvicorn
825rps, resp time 1-2ms
https://overload.yandex.net/423686

### Go 
10000rps, resp time 0.1-1ms
https://overload.yandex.net/423681


# Big json hash
### Py3.8 simple http
1300rps, resp time 1-10ms
https://overload.yandex.net/423677

### Py3.8 fastapi + 1proc uvicorn
640rps, resp time 1-10ms
https://overload.yandex.net/423674

### Go 
500rps, resp time 8-100ms
https://overload.yandex.net/423678