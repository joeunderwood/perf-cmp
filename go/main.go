package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

var data, _ = getData()

func getData() (map[string]interface{}, error) {
	file, err := ioutil.ReadFile("../data.json")

	var showcase map[string]interface{}
	err = json.Unmarshal([]byte(file), &showcase)

	log.Println(showcase)

	return showcase, err
}

func getDataHash() (string, error) {
	output, err := json.Marshal(data)

	return fmt.Sprintf("%x\n", md5.Sum(output)), err
}

func process(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "POST":
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var payload map[string]interface{}
		err = json.Unmarshal([]byte(body), &payload)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// hash, err := getDataHash()

		io.WriteString(w, "ok")
		// log.Println(hash)

	default:
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
	}
}

func main() {

	http.HandleFunc("/", process)

	fmt.Printf("Starting server at port 8000\n")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
